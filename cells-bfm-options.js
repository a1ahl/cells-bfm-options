{
  const {
    html,
  } = Polymer;

    /**
      `<cells-bfm-options>` Description.

      Example:

      ```html
      <cells-bfm-options></cells-bfm-options>
      ```

      ## Styling
      The following custom properties and mixins are available for styling:

      ### Custom Properties
      | Custom Property     | Selector | CSS Property | Value       |
      | ------------------- | -------- | ------------ | ----------- |
      | --cells-fontDefault | :host    | font-family  |  sans-serif |
      ### @apply
      | Mixins    | Selector | Value |
      | --------- | -------- | ----- |
      | --cells-bfm-options | :host    | {} |

      * @customElement
      * @polymer
      * @extends {Polymer.Element}
      * @demo demo/index.html
    */
  class CellsBfmOptions extends Polymer.Element {

    static get is() {
      return 'cells-bfm-options';
    }

    static get properties() {
      return {
        content: {
          type: Array,
          value: () => ([])
        },
        event: {
          type: String,
          value: ''
        }
      };
    }

    _isEqualTo(itemType, type) {
      return itemType === type;
    }

    _setEventButton(e) {
      this.dispatchEvent(new CustomEvent(`${e.model.__data.item.event}-event`, {
        bubbles: true,
        composed: true
      }));
    }

  }
  customElements.define(CellsBfmOptions.is, CellsBfmOptions);
}